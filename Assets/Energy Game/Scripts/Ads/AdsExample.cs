﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Opendream.PTT
{
    public class AdsExample : MonoBehaviour
    {
        public void ShowAds()
        {
            Ads.Instance.ShowAds(GetRewards);
        }

        public void GetRewards(AdsItem item, bool endOfVideo)
        {
            Debug.Log("Get rewards from ads");
            Debug.Log(item);
            Debug.Log("endOfVideo: " + endOfVideo);
        }
    }
}
