﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Opendream.PTT
{
    public class AdsItem
    {
        public string id;
        public string url;
        public string type;
        public int width;
        public int height;
        public int duration;
        public int video_length;

        public override string ToString()
        {
            return JsonUtility.ToJson(this);
        }
    }
}
