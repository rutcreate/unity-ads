﻿using System.Collections;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using UnityEngine.Video;
using UnityEngine.Networking;
using TMPro;

namespace Opendream.PTT
{
    public class Ads : MonoBehaviour
    {
        [SerializeField]
        private string m_Endpoint;

        [SerializeField]
        private VideoPlayer m_VideoPlayer;

        [SerializeField]
        private RawImage m_Renderer;

        [SerializeField]
        private RectTransform m_Content;

        [SerializeField]
        private TextMeshProUGUI m_DurationText;

        [SerializeField]
        private Button m_CloseButton;

        [Header("Mute Video")]

        [SerializeField]
        private bool m_MuteOnStart;

        [SerializeField]
        private Button m_MuteButton;

        [SerializeField]
        private Button m_UnmuteButton;

        [SerializeField]
        private RectTransform m_Loading;

        private bool m_VideoLoaded;

        private Texture m_LocalTexture;

        public delegate void OnWatchAdsEndDelegate(AdsItem item, bool endOfVideo);

        private OnWatchAdsEndDelegate m_OnWatchAdsEnd;

        private AdsItem m_CurrentAdsItem;

        private bool m_IsEndOfVideo;

        private static Ads s_Instance;

        public static Ads Instance
        {
            get
            {
                if (s_Instance == null)
                {
                    s_Instance = FindObjectOfType<Ads>();
                }

                return s_Instance;
            }
        }

        private void Start()
        {
            HideContent();
            HideLoading();
            HideRenderer();

            m_LocalTexture = m_Renderer.texture;

            m_VideoPlayer.loopPointReached += VideoPlayer_loopPointReached;
            m_VideoPlayer.started += VideoPlayer_started;

            DontDestroyOnLoad(gameObject);
        }

        public void Mute()
        {
            m_MuteButton.gameObject.SetActive(false);
            m_UnmuteButton.gameObject.SetActive(true);
            m_VideoPlayer.SetDirectAudioVolume(0, 0f);
            m_MuteOnStart = true;
        }

        public void Unmute()
        {
            m_MuteButton.gameObject.SetActive(true);
            m_UnmuteButton.gameObject.SetActive(false);
            m_VideoPlayer.SetDirectAudioVolume(0, 1f);
            m_MuteOnStart = false;
        }

        public void ShowAds(OnWatchAdsEndDelegate onWatchAdsEnd)
        {
            m_OnWatchAdsEnd = onWatchAdsEnd;
            StartCoroutine(StartShowAds());
        }

        public void CloseAds()
        {
            if (m_OnWatchAdsEnd != null)
            {
                m_OnWatchAdsEnd(m_CurrentAdsItem, m_IsEndOfVideo);
            }

            HideContent();
        }

        public void ShowContent()
        {
            m_Content.gameObject.SetActive(true);
        }

        public void HideContent()
        {
            m_Content.gameObject.SetActive(false);
        }

        public void ShowLoading()
        {
            m_Loading.gameObject.SetActive(true);
        }

        public void HideLoading()
        {
            m_Loading.gameObject.SetActive(false);
        }

        public void ShowRenderer()
        {
            m_Renderer.gameObject.SetActive(true);
        }

        public void HideRenderer()
        {
            m_Renderer.gameObject.SetActive(false);
        }

        public IEnumerator StartShowAds()
        {
            ShowContent();

            m_CloseButton.gameObject.SetActive(false);
            m_MuteButton.gameObject.SetActive(false);
            m_UnmuteButton.gameObject.SetActive(false);
            m_DurationText.gameObject.SetActive(false);

            ShowLoading();
            HideRenderer();

            m_Renderer.texture = m_LocalTexture;
            m_IsEndOfVideo = false;

            using (UnityWebRequest request = UnityWebRequest.Get(m_Endpoint))
            {
                yield return request.SendWebRequest();

                if (request.isNetworkError || request.isHttpError)
                {
                    Debug.Log(request.error);
                }
                else
                {
                    AdsItem item = JsonUtility.FromJson<AdsItem>(request.downloadHandler.text);

                    m_CurrentAdsItem = item;

                    UpdateRendererSize(item.width, item.height);

                    if (item.type == "image")
                    {
                        Debug.Log("Image: " + item.url);
                        yield return ShowImageAds(item);
                    }
                    else if (item.type == "video")
                    {
                        if (m_MuteOnStart)
                        {
                            Mute();
                        }
                        else
                        {
                            Unmute();
                        }
                        Debug.Log("Video: " + item.url);
                        yield return ShowVideoAds(item);
                    }

                    StartCoroutine(StartCountDown(item.duration));

                    yield return new WaitForSeconds(item.duration);

                    m_CloseButton.gameObject.SetActive(true);
                }
            }
        }

        private IEnumerator StartCountDown(int duration)
        {
            m_DurationText.gameObject.SetActive(true);

            UpdateCounter(duration);

            while (duration > 0)
            {
                yield return new WaitForSeconds(1f);
                duration--;
                UpdateCounter(duration);
            }

            m_DurationText.gameObject.SetActive(false);
        }

        private void UpdateCounter(int duration)
        {
            m_DurationText.text = duration.ToString();
        }

        private IEnumerator ShowImageAds(AdsItem item)
        {
            using (UnityWebRequest request = UnityWebRequestTexture.GetTexture(item.url))
            {
                yield return request.SendWebRequest();
                HideLoading();
                ShowRenderer();
                m_Renderer.texture = ((DownloadHandlerTexture)request.downloadHandler).texture;
            }
        }

        private void UpdateRendererSize(int width, int height)
        {
            float aspectRatio = width * 1f / height * 1f;
            Vector2 size = m_Renderer.rectTransform.sizeDelta;
            size.y = size.x / aspectRatio;
            m_Renderer.rectTransform.sizeDelta = size;
        }

        public void ChangeUrl(string url)
        {
            m_VideoPlayer.url = url;
        }

        private IEnumerator ShowVideoAds(AdsItem item)
        {
            m_VideoPlayer.url = item.url;
            m_VideoPlayer.Play();

            m_VideoLoaded = false;

            while (!m_VideoLoaded)
            {
                yield return new WaitForEndOfFrame();
            }

            ClearRenderTexture(m_VideoPlayer.targetTexture);
        }

        private void VideoPlayer_loopPointReached(VideoPlayer source)
        {
            m_CloseButton.gameObject.SetActive(true);
            m_IsEndOfVideo = true;
        }

        private void VideoPlayer_started(VideoPlayer source)
        {
            m_VideoLoaded = true;
            HideLoading();
            ShowRenderer();
        }

        private void ClearRenderTexture(RenderTexture renderTexture)
        {
            RenderTexture rt = RenderTexture.active;
            RenderTexture.active = renderTexture;
            GL.Clear(true, true, Color.clear);
            RenderTexture.active = rt;
        }
    }
}
